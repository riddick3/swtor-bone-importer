if (heapSize < 20000000) then
	heapSize = 200000000
fname = getOpenFileName \
caption:"Open .gr2 from Skeleton folder" \
types:"SWTOR_SKEL (*_skeleton.gr2)|*_skeleton.gr2" \
historyCategory:"SWTOR_SKELObjectPresets"
f = fopen fname "rb"


fn readHalfFloat fstream = (
    hf=readshort fstream #unsigned
    sign = bit.get hf 16
    exponent = (bit.shift (bit.and hf (bit.hexasint "7C00")) -10) as integer - 16
    fraction = bit.and hf (bit.hexasint "03FF")
    if sign==true then sign = 1 else sign = 0
    exponentF = exponent + 127
    outputAsFloat = bit.or (bit.or (bit.shift fraction 13) \
    (bit.shift exponentF 23)) (bit.shift sign 31)
    return bit.intasfloat outputasfloat*2
    )

fn ReadFixedString bstream fixedLen =
(
	local str = ""
	for i = 1 to fixedLen do
	(
		str += bit.intAsChar (ReadByte bstream #unsigned)
	)
	str
)	

clearlistener()

BNArr = #()   
BoneNamesArray=#()	
fseek f 0x1C #seek_set
BCount=readlong f
print BCount

fseek f 0x70 #seek_set
for a = 1 to BCount do (
getPos = ftell f + 136	
BoneNameOffset = readlong f
fseek f getPos #seek_set	
Pos=ftell f	
fseek f BoneNameOffset #seek_set	
BoneName= readstring f
fseek f Pos #seek_set	
append BoneNamesArray BoneName	
)	

fseek f 0x70 #seek_set
for a = 1 to BCount do (
BoneNameOffset = readlong f #unsigned
BoneParent	= readlong f 
a11 = Readfloat f; a12 = Readfloat f; a13 = Readfloat f; a14 = Readfloat f
a21 = Readfloat f; a22 = Readfloat f; a23 = Readfloat f; a24 = Readfloat f
a31 = Readfloat f; a32 = Readfloat f; a33 = Readfloat f; a34 = Readfloat f	
a41 = Readfloat f; a42 = Readfloat f; a43 = Readfloat f; a44 = Readfloat f
	
c11 = Readfloat f; c12 = Readfloat f; c13 = Readfloat f; c14 = Readfloat f
c21 = Readfloat f; c22 = Readfloat f; c23 = Readfloat f; c24 = Readfloat f
c31 = Readfloat f; c32 = Readfloat f; c33 = Readfloat f; c34 = Readfloat f	
c41 = Readfloat f; c42 = Readfloat f; c43 = Readfloat f; c44 = Readfloat f	
	
tfm = matrix3 [c11,c13,c12] [c21,c23,c22] [c31,c33,c32] ([c41,c43,c42]*1000)
	
	newBone = bonesys.createbone	\
				  tfm.row4	\
				  (tfm.row4 + 0.01 * (normalize tfm.row1)) \
				  (normalize tfm.row3)
			newBone.name = BoneNamesArray[a]
			newBone.width  = 0.01
			newBone.height = 0.01
			newBone.transform =  inverse tfm		  
			newBone.setBoneEnable false 0
			newBone.wirecolor = white
			newbone.showlinks = true
			newBone.pos.controller      = TCB_position ()
			newBone.rotation.controller = TCB_rotation ()
if (boneparent != -1) then
 newBone.parent = BNArr[(boneparent + 1)]							  
append BNArr newBone
)	

Print ("Bones Sec End @ 0x"+((bit.intAsHex(ftell f))as string))	
 
fclose f